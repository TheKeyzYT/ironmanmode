package de.thekeyz.commands;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.thekeyz.main.Main;
import de.thekeyz.manager.LocManager;

public class Loc_CMD implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(sender instanceof Player) {
			if(sender.hasPermission("system.locations") || sender.isOp()) {
				Player p = (Player) sender;
				if(args.length == 0) {
					p.sendMessage("➤");
					p.sendMessage(Main.prefix+"/loc create [name]");
					p.sendMessage(Main.prefix+"/loc teleport [name]");
					p.sendMessage(Main.prefix+"/loc delete [name]");
					p.sendMessage("➤");
				}else if(args.length == 1) {
					p.sendMessage(Main.prefix+"Bitte gib den Name der Location noch an");
					if(args[0].equalsIgnoreCase("create") || args[0].equalsIgnoreCase("c")) {
						LocManager.setLocation(args[1], p.getLocation());
						p.sendMessage(Main.prefix+"Du hast erfolgreich die Location §6"+args[1]+" §ferstellt");
					}else if(args[0].equalsIgnoreCase("teleport") || args[0].equalsIgnoreCase("tp")) {
						Location loc = LocManager.getLocation(args[1]);
						if(loc == null) {
							p.sendMessage(Main.prefix+"Die Location existiert nicht");
						}else {
							p.teleport(loc);
							p.sendMessage(Main.prefix+"Du hast dich erfolgreich zu der Location §6"+args[1]+" §fteleportiert");
						}
					}else {
						p.sendMessage("➤");
						p.sendMessage(Main.prefix+"/loc create [name]");
						p.sendMessage(Main.prefix+"/loc teleport [name]");
						p.sendMessage(Main.prefix+"/loc delete [name]");
						p.sendMessage("➤");
					}
				}
			}
		}else {
			Bukkit.getConsoleSender().sendMessage(Main.prefix+"Du musst ein Spieler sein");
		}
		
		return false;
	}
	
	

}
