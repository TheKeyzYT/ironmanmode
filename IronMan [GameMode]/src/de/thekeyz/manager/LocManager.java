package de.thekeyz.manager;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import de.thekeyz.main.Main;

public class LocManager {

	public static File file = Main.file;
	public static FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
	
	public static boolean setLocation(String path, Location loc) {
		
		cfg.set(path+".world", loc.getWorld().getName());
		cfg.set(path+".x", loc.getX());
		cfg.set(path+".y", loc.getY());
		cfg.set(path+".z", loc.getZ());
		cfg.set(path+".yaw", loc.getYaw());
		cfg.set(path+".pitch", loc.getPitch());
		boolean rt = save();
		return rt;
		
	}
	public static boolean deleteLocation(String path, Location loc) {
		
		cfg.set(path+".world", null);
		cfg.set(path+".x", null);
		cfg.set(path+".y", null);
		cfg.set(path+".z", null);
		cfg.set(path+".yaw", null);
		cfg.set(path+".pitch", null);
		boolean rt = save();
		return rt;
		
	}
	
	public static Location getLocation(String path) {
		Location loc = null;
		if(cfg.contains(path+".world")) {
			World w = Bukkit.getWorld(cfg.getString(path+".world"));
			double x = cfg.getDouble(path+".x");
			double y = cfg.getDouble(path+".y");
			double z = cfg.getDouble(path+".z");
			double yaw = cfg.getDouble(path+".yaw");
			double pitch = cfg.getDouble(path+"pitch");
			loc = new Location(w, x, y, z, (float)yaw, (float)pitch);
			
		}
		return loc;
		
	}
	
	public static boolean save() {
		try {
			cfg.save(file);
			return true;
		}catch(IOException e) {
			e.printStackTrace();
			return false;
		}
		
	}
	

	
}
