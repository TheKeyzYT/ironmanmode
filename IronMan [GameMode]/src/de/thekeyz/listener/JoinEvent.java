package de.thekeyz.listener;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import de.thekeyz.main.Main;

public class JoinEvent implements Listener {
	
	private Main plugin;
	public JoinEvent(Main plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		
		Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
			
			@Override
			public void run() {
				Bukkit.broadcastMessage("§6➤");
				Bukkit.broadcastMessage(Main.prefix+"Ein Spieler wurde nun in die Runde gesendet");
				Bukkit.broadcastMessage("§6➤");
				
				//Action Player Teleport
			}
		}, 0, 30*20);
		
		ItemStack itm = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
		ItemMeta meta = itm.getItemMeta();
		meta.setDisplayName("§6§lKits");
		itm.setItemMeta(meta);
		
		p.playSound(p.getLocation(), Sound.LEVEL_UP, 10, 2);
		e.setJoinMessage("[§a+§f] "+p.getName());
		p.sendMessage(Main.prefix+"Wähle ein Kit aus");
		
		p.getInventory().setItem(4, itm);
	}
	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		Player p = e.getPlayer();
		
		e.setQuitMessage("[§c-§f] "+p.getName());
	}
	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		e.setCancelled(true);
		
		
	}
	public void randomPlayerTP(Player p) {
		
	}
	
	
	
}
